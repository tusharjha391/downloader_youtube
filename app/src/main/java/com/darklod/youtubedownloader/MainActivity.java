package com.darklod.youtubedownloader;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import at.huber.youtubeExtractor.YouTubeUriExtractor;
import at.huber.youtubeExtractor.YtFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private List<Item> videos = new ArrayList<>();
    private RecyclerViewAdapter adapter;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private SearchBox search;
    private String searchText = "";
    private EditText mSearchEdit;
    private LinearLayout mSearchLayout;
    private final static int MAX_RESULTS = 25;
    private static final int NO_AUDIO_BITRATE_SUPPORT = -1;
    private TextView mPoweredByText;
    private TextView mDialogHeadingText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDialogHeadingText = findViewById(R.id.dialog_heading_text);
        mPoweredByText = findViewById(R.id.powered_by_text);


        mDialogHeadingText.setTypeface(Typeface.createFromAsset(getAssets(), "lato_regular.ttf"));

        search = findViewById(R.id.searchbox);
        toolbar = findViewById(R.id.toolbar);
        mSearchEdit = findViewById(R.id.search_edit);
        mSearchLayout = findViewById(R.id.search_layout);
        CardView mSearchButton = findViewById(R.id.search_button);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchTerm = mSearchEdit.getText().toString().trim();
                if (searchTerm.isEmpty()) return;
                mSearchLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                performSearch(searchTerm, MAX_RESULTS);
                hideKeyboard();
            }
        });

        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.toolbarText));
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.action_search && !search.isShown()) {
                    openSearch();
                    return true;
                }
                return false;
            }
        });

        adapter = new RecyclerViewAdapter(this, videos);
        adapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Item item) {
                String youtubeLink = "http://youtube.com/watch?v=" + item.id.videoId;

                @SuppressLint("StaticFieldLeak") YouTubeUriExtractor ytEx = new YouTubeUriExtractor(getApplicationContext()) {
                    @Override
                    public void onUrisAvailable(String videoId, String videoTitle, SparseArray<YtFile> ytFiles) {
                        if (ytFiles != null) {
                            List<YtFile> allMp4Formats = new ArrayList<>();
                            for (int i = 0; i < ytFiles.size(); i++) {
                                int key = ytFiles.keyAt(i);
                                YtFile file = ytFiles.get(key);
                                if (file.getFormat() != null && file.getFormat().getExt() != null && file.getFormat().getExt().equalsIgnoreCase("mp4")) {
                                    allMp4Formats.add(file);
                                }
                            }


                            if (allMp4Formats.isEmpty()) {
                                Toast.makeText(MainActivity.this, "Mp4 format is not available!", Toast.LENGTH_LONG).show();
                                return;
                            }

                            Collections.sort(allMp4Formats, new Comparator<YtFile>() {
                                @Override
                                public int compare(YtFile y1, YtFile y2) {
                                    if (y1.getFormat().getHeight() > y2.getFormat().getHeight() && y1.getFormat().getAudioBitrate() != -1)
                                        return 1;
                                    else if (y1.getFormat().getHeight() < y2.getFormat().getHeight())
                                        return -1;
                                    else return 0;
                                }
                            });

                            String ytFileWithAVDownloadUrl = null;
                            for (YtFile ytFile : allMp4Formats) {
                                if (ytFile.getFormat().getAudioBitrate() != NO_AUDIO_BITRATE_SUPPORT) {
                                    ytFileWithAVDownloadUrl = ytFile.getUrl();
                                }
                            }

                            if (ytFileWithAVDownloadUrl == null) {
                                Toast.makeText(MainActivity.this, "No supported download format found for this video!", Toast.LENGTH_LONG).show();
                                return;
                            }

                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(ytFileWithAVDownloadUrl));
                            request.setDescription("");
                            request.setTitle("Downloading: " + videoTitle);
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, videoTitle + ".mp4");

                            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                            if (manager != null) {
                                manager.enqueue(request);
                                Toast.makeText(MainActivity.this, "Video is downloading! Check the notification area", Toast.LENGTH_LONG).show();
                            }
                            else
                                Toast.makeText(MainActivity.this, "Sorry, error occurred!", Toast.LENGTH_LONG).show();
                        }
                    }
                };
                ytEx.execute(youtubeLink);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        progressBar = findViewById(R.id.progress_bar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.colorAccent),
                        PorterDuff.Mode.SRC_IN);
        progressBar.setVisibility(View.GONE);

        loadLibrary();
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromWindow(search.getApplicationWindowToken(), 0);
    }


    private void openSearch() {
        search.revealFromMenuItem(R.id.action_search, this);
        search.setSearchListener(new SearchBox.SearchListener() {

            @Override
            public void onSearchOpened() {
                // Use this to tint the screen
            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                closeSearch();
                hideKeyboard();
            }

            @Override
            public void onSearchTermChanged(String term) {
                // React to the search term changing
                // Called after it has updated results
            }

            @Override
            public void onSearch(String searchTerm) {
                toolbar.setTitle("Results for: " + searchTerm);
                progressBar.setVisibility(View.VISIBLE);
                mSearchLayout.setVisibility(View.GONE);
                performSearch(searchTerm, MAX_RESULTS);
            }

            @Override
            public void onResultClick(SearchResult result) {
                //React to result being clicked
            }

            @Override
            public void onSearchCleared() {

            }
        });

    }

    private void closeSearch() {
        search.hideCircularlyToMenuItem(R.id.action_search, this);
        //if (search.getSearchText().isEmpty()) toolbar.setTitle("");
    }

    private void performSearch(String text, int maxResults) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<SearchInfo> call = apiService.getVideosByKeyword("snippet", text, maxResults, ApiClient.API_KEY);
        call.enqueue(new Callback<SearchInfo>() {
            @Override
            public void onResponse(@NonNull Call<SearchInfo> call, @NonNull Response<SearchInfo> response) {
                SearchInfo info = response.body();

                videos.clear();
                if (info != null) {
                    videos.addAll(info.items);
                }

                adapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<SearchInfo> call, @NonNull Throwable t) {
                // Log error here since request failed
                Log.e("PROVA", t.toString());
            }
        });
    }

    private void loadLibrary() {
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                }

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            Toast.makeText(getApplicationContext(), "Mp3 conversion is not supported by that device", Toast.LENGTH_LONG).show();
        }
    }

    private void ExecuteCommand(String[] cmd) {
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onProgress(String message) {
                }

                @Override
                public void onFailure(String message) {
                }

                @Override
                public void onSuccess(String message) {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            Toast.makeText(getApplicationContext(), "Already running", Toast.LENGTH_LONG).show();
        }
    }

    private void convertToMp3() {
        String[] cmds = new String[]{"a", "b"};
        ExecuteCommand(cmds);

        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
        // ffmpeg.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

