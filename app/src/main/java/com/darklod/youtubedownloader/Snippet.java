package com.darklod.youtubedownloader;

import java.util.Date;

public class Snippet
{
    public Date publishedAt;
    public String channelId;
    public String title;
    String description;
    Thumbnails thumbnails;
    String channelTitle;
    public String liveBroadcastContent;
}
